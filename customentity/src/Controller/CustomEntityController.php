<?php

namespace Drupal\customentity\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class CustomEntityController
 * 
 * Ορίζει έναν Controller για τη διαχείριση του περιεχομένου 
 * του custom module μας.
 */

class CustomEntityController extends ControllerBase {

  /**
    * Class CustomEntityController
    * 
    * Επιστρέφει έναν πίνακα με δυνατότητα εμφάνισης για τη
    * δοκιμαστική σελίδα του custom module μας.
    */
  public function content() {
    $build = [
      '#markup' => $this->t('Hello World!'),
    ];
    return $build;
  }
}