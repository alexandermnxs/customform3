<?php

/**
  * @file
  * Contains \Drupal\customentity\EidosServices
  */

namespace Drupal\customentity;

use Drupal\Core\Entity;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Class EidosServices
 * 
 * Ορίζει ένα service για τη διεχείριση του τύπου περιεχομένου Είδος
 */
class EidosServices {
  
  /** 
   * Constructor  
   */
  public function __construct() {
  }

  /**
   * Ορίζει το συγκεκριμένο node του Είδους 
   * και επιστρέφει τον Βασικό Κωδικό του τύπου περιεχομένου Είδος
   * 
   * @param string $node
   *   Ορίζει το node: $node του Είδος
   * @return string
   * 
   */
  public function preSave(NodeInterface $node) {
    $node->bundle();
    $kodikos = $node->get('field_basikos_kodikos')->getString();
    return $kodikos;
  }
}