<?php

/**
  * @file
  * Contains \Drupal\customentity\KinhshParServices
  */

namespace Drupal\customentity;

use Drupal\Core\Entity;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\NodeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class KinhshParServices
 * 
 * Ορίζει τo κατάλληλο service για τη διεχείριση του τύπου περιεχομένου Κίνηση Παραστατικού
 */
class KinhshParServices {

  /** 
    * @var Drupal\Core\Entity\EntityTypeManager
    */
  protected $entityTypeManager;

  /**
   * @var Drupal\node 
   */
  protected $nodeStorage;

  /**
   * Constructor
   * 
   * @param $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
  }

  /**
   * 
   * Υπολογίζεται το Τελικό Ποσό με βάση το πεδίο Τιμή(Είδος) 
   * και Ποσότητα(Κίνηση Παραστατικού)
   * και επιστρέφεται το Τελικό Ποσό
   * του τύπου περιεχομένου Κίνηση Παραστατικού
   * 
   * @param string $node
   *   Ορίζει το node: $node της Κίνησης Παραστατικού
   * @param string $eidos_data
   *   Ορίζει το node: $eidos_data του Είδους 
   * @return string
   * 
   */
  public function preSave1(NodeInterface $node) {
    $node->id();  

    $eidos = $node->get('field_eidos')->getString();
    $eidos_data = $this->entityTypeManager->getStorage('node')->load($eidos);
    $timi = $eidos_data->get('field_timi')->getString();

    $posotita = $node->get('field_posotita')->getString();
    $ypologismos = $posotita * $timi;
    $teliko_poso = $node->set('field_teliko_poso', $ypologismos);

    return $teliko_poso;
  }
}